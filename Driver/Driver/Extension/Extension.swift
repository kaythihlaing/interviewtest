//
//  Extension.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import UIKit

extension UITextField {
func setIcon(_ image: UIImage) {
   let iconView = UIImageView(frame:
                  CGRect(x: 10, y: 5, width: 20, height: 20))
   iconView.image = image
   let iconContainerView: UIView = UIView(frame:
                  CGRect(x: 20, y: 0, width: 30, height: 30))
   iconContainerView.addSubview(iconView)
   leftView = iconContainerView
   leftViewMode = .always
}
}
