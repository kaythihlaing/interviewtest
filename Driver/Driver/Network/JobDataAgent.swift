//
//  JobDataAgent.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import Foundation

class JobDataAgent {
    
    private static var sharedNetworkManager: JobDataAgent = {
        return JobDataAgent()
    }()
    
    class func shared() -> JobDataAgent {
        return sharedNetworkManager
    }
    
    func getJobList(success : @escaping ([JobData]) -> Void, failure : @escaping (String) -> Void) {
        
        NetworkManager.shared().getDataArray(url: "https://www.haulio.io/joblist.json", type: JobData.self, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}
