//
//  NetworkManager.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class NetworkManager {
    
    
    //MARK: - Singleton
    private static var sharedNetworkManager: NetworkManager = {
        return NetworkManager()
    }()
    
    private init() {}
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    //MARK: - Cancel all previous requests
    public func cancelAllRequest() {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({$0.cancel()})
        }
    }
    
    //MARK: - Network Request
    public func getDataArray<T: Mappable>(url: String,
                        type: T.Type,
                        success : @escaping ([T]) -> Void,
                        failure : @escaping (String) -> Void) {
        
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                case .success:
                    guard let result = response.result.value else {
                        failure("Response Error")
                        return
                    }
                    
                    let resultArray : [T] = Mapper<T>().mapArray(JSONObject: result) ?? []
                    
                    success(resultArray)
                
                case .failure(let error):
                    failure(error.localizedDescription)
            }
        }
    }
    
}

