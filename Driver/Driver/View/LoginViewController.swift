//
//  LoginViewController.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import UIKit
import SnapKit
import GoogleSignIn

class LoginViewController: UIViewController {
    
    lazy var naviBgView: UIView = {
       let view = UIView()
       view.translatesAutoresizingMaskIntoConstraints = false
       view.backgroundColor = UIColor.red
       return view
    }()
    
    lazy var btnBarView: UIView = {
       let view = UIView()
       view.translatesAutoresizingMaskIntoConstraints = false
       view.backgroundColor = UIColor.red
       return view
    }()
    
    lazy var ivLogo: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ss")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    lazy var signView: UIView = {
       let view = UIView()
       view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 0.5
       return view
    }()
    
    
    lazy var ivGoogle: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "G")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblView: UIView = {
       let view = UIView()
       view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.systemBlue
       return view
    }()
    
    lazy var googleBtn: GIDSignInButton = {
        let btn = GIDSignInButton()
        //btn.setTitle("Sign up with Google", for: .normal)
        btn.backgroundColor = UIColor.systemBlue
        //btn.addTarget(self, action: #selector(didTapBtnSign), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        setupUI()
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSignIn), name: NSNotification.Name("SuccessfulSignInNotification"), object: nil)

    }
    @objc func didSignIn()  {
         let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "JobListViewController") as! JobListViewController
        self.navigationController?.pushViewController(vc, animated: true)
      

    }
    
    private func setupUI() {
        
        view.addSubview(naviBgView)
        view.addSubview(ivLogo)
        view.addSubview(googleBtn)
//        signView.addSubview(ivGoogle)
        view.addSubview(btnBarView)
//        signView.addSubview(btnSign)
        setConstraints()
    }
    
    private func setConstraints() {
    
        naviBgView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(30)
        }
        
        ivLogo.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-70)
        }
        
//        signView.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.width.equalTo(250)
//            make.height.equalTo(40)
//            make.top.equalTo(ivLogo.snp.bottom).offset(30)
//        }
        
//        ivGoogle.snp.makeConstraints { (make) in
//            make.centerY.equalToSuperview()
//            make.width.equalTo(30)
//            make.height.equalTo(30)
//            make.leading.equalToSuperview().offset(5)
//        }
        
        btnBarView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(30)
        }
        
        googleBtn.snp.makeConstraints { (make) in
            make.top.equalTo(ivLogo.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}
