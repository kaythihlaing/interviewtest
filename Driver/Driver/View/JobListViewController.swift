//
//  JobListViewController.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class JobListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
    override func viewDidLoad() {
        fetchData()
        tableView.estimatedRowHeight = 130
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    func fetchData() {
        JobModel.shared().getJobList(success: {
            self.tableView.reloadData()
        }) { (error) in
            print("ERROR => \(error)")
        }
    }
    
    @objc func didTapBtnAccept(sender: UIButton) {
        JobModel.shared().selectJob = JobModel.shared().jobList[sender.tag]
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logoutTapped(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension JobListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "JobListTableViewCell", for: indexPath) as? JobListTableViewCell {
            //cell.setData(JobModel.shared().jobList[indexPath.row])
            let data = JobModel.shared().jobList[indexPath.row]
            cell.lblJob.text = "Job Number : " + String(data.jobId ?? 1)
            cell.lblCom.text = "Company : " + (data.company ?? "")
            cell.lblAdd.text = "Address : " + (data.address ?? "")
            cell.btnAcc.tag = indexPath.row
            cell.btnAcc.layer.cornerRadius = 4
            cell.btnAcc.addTarget(self, action: #selector(didTapBtnAccept(sender:)), for: .touchUpInside)
            
            return cell
        }
        
        return UITableViewCell()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JobModel.shared().jobList.count
    }
}
