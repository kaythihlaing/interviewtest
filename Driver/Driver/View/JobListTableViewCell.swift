//
//  JobListTableViewCell.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import UIKit

class JobListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblJob: UILabel!
    @IBOutlet weak var lblCom: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var btnAcc: UIButton!
    
}
