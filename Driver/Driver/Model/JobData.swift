//
//  JobData.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import ObjectMapper

struct JobData: Mappable{
    var id : Int?
    var jobId : Int?
    var priority : Int?
    var company : String?
    var address : String?
    var geolocation : Geolocation?
    
    init() {
        
    }

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        jobId <- map["job-id"]
        priority <- map["priority"]
        company <- map["company"]
        address <- map["address"]
        geolocation <- map["geolocation"]
    }

}

struct Geolocation : Mappable {
    var latitude : Double?
    var longitude : Double?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }

}
