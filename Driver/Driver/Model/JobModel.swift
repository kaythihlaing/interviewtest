//
//  JobModel.swift
//  Driver
//
//  Created by Kay Thi Hlaing on 13/10/2020.
//  Copyright © 2020 Kay Thi Hlaing. All rights reserved.
//

import Foundation
class JobModel {

    var jobList : [JobData] = []
    var selectJob : JobData = JobData()
    var username : String = ""
    
    private init() {}
    
    class func shared() -> JobModel {
        return sharedDataModel
    }
    
    private static var sharedDataModel: JobModel = {
        let dataModel = JobModel()
        return dataModel
    }()
    
    func getJobList(success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        JobDataAgent.shared().getJobList(success: { (data) in
            self.jobList = data
            success()
        }) { (error) in
            failure(error)
        }
    }
}
